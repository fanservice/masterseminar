const userdatabase = require('../User/userdatabase')
const CREATEUSER = require('../User/CreateUser')

function initDatabase(database) {

    const userDB = new userdatabase();
    const createUser = new CREATEUSER(userDB);
    const test = createUser.createUsersAndPutToDatabase().then(d => {
    })
    return async function init(req, res, next) {
        req.database = await userDB.getDatabase();
        next()
    }
}

module.exports = initDatabase
