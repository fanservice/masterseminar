var express = require('express');
const fs = require('fs')
const key = fs.readFileSync("keys/mykeys.pub")

var path = require('path');
var logger = require('morgan');
const initDatabase = require('./middleware/initDatabase')
const loadkey = require('./jwt/loadkey')

var indexRouter = require('./routes/authentication');

var app = express();


app.use(logger('dev'));
app.use(initDatabase("§"));
app.use(loadkey(key));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api', indexRouter);
module.exports = app


