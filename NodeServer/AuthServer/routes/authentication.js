var express = require('express');
var router = express.Router();
const Authentication = require("../jwt/authentication")

/* GET home page. */
router.post('/authentication', async (req, res, next) => {
        try {
            const auth = new Authentication(req.keyfile);
            const userNameAndPass = [req.body.user, req.body.pass];
            const database = req.database;
            const user = await database.getUserFromDataBase(userNameAndPass)
            if (user) {
                const jwt = await auth.signeUser(user)
                if (jwt) {
                    res.status(200)
                    res.send(jwt)
                } else {
                    res.status(551)
                    res.send("could not sign")
                }
            } else {
                res.status(551)
                res.send("user not found")
            }

        } catch (e) {
            res.status(551)
            res.send("<p>auth error</p>")
        }
        next();
    }
);

module.exports = router;
