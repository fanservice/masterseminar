class Userdatabase {

    constructor() {
        this.database = []
    }

    async addUserToDataBase(user) {
        if (Array.isArray(user)) {
            user.forEach(d => this.database.push(d))
        } else {
            this.database.push(user)
        }
    }

    async getUserFromDataBase(usernameAndPassword) {
        try {
            let user = {}
            if (Array.isArray(usernameAndPassword)) {
                for (const d of this.database) {
                    await d.getUser(usernameAndPassword);
                    if (await d.getUser(usernameAndPassword)) {
                        return d
                    }
                }
            }
        } catch (e) {
            return "undefined"
        }
    }

    async getDatabase() {
        return this
    }
}

module.exports = Userdatabase;
