const User = require('./user')

class CreateUser {

    constructor(userdatabase) {
        this.userdatabase = userdatabase;
    }

    async createUsersAndPutToDatabase() {
        const user1 = new User("kevin");
        await user1.setPassword("kevin")
        await user1.addRole(["post", "get", "put"])
        const user2 = new User("admin");
        await user2.setPassword("admin")
        await user2.addRole(["admin", "post", "get", "put"])
        await this.userdatabase.addUserToDataBase([user1, user2])
        return "done"
    }

}

module.exports = CreateUser;
