const bcrypt = require('bcrypt');

class User {

    constructor(username) {
        this.username = username
        this.password = ""
        this.role = [];
    }


    async setPassword(password) {
        this.password = await bcrypt.hash(password, 10)

    }

    async addRole(role) {
        if (Array.isArray(role)) {
            role.forEach(d => this.role.push(d))
        } else {
            this.role.push(role)
        }
    }


    async getUser(usernameAndPassword) {
        return !!(this.username === usernameAndPassword[0] && await bcrypt.compareSync(usernameAndPassword[1], this.password));
    }
}

module.exports = User;
