const jose = require('jose')

class Authentication {

    constructor(keyfile) {
        this.publickey = jose.JWK.asKey(keyfile);

    }

    async signeUser(user) {
        try {
            const tobeSigned = {user}
            const jwt = jose.JWT.sign(tobeSigned, this.publickey)
            const isSigned = jose.JWT.verify(jwt, this.publickey)
            return jwt;

        } catch (e) {

            return e
        }

    }
}

module.exports = Authentication;
