var express = require('express');
var router = express.Router();
const request = require('axios')


/* GET home page. */
router.post('/', async (req, res, next) => {
    try {
        const header = {
            headers: {
                Host: "auth"
            }
        }
        const user = req.body.user
        const password = req.body.pass
        const options = {
            "user": user,
            "pass": password
        }

        const token = await request.post("http://traefik_reverse-proxy_1/api/authentication", options, header)
        if (token && token.status === 200) {
            res.send({"token": token.data});
        } else {
            res.status(token.status)
            res.send(token.data)
        }

    } catch (e) {
        res.status(551);
        res.send("<p>auth error</p>")
    }
});

module.exports = router;
