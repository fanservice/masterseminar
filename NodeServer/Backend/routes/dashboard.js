var express = require('express');
var router = express.Router();
const axios = require('axios')

/* GET home page. */
router.get('/users', async (req, res, next) => {

    const options = {
        params: {
            token: req.query.token
        },
        headers: {
            Host: "core"
        }
    }
    try {
        const data = await axios.get("http://traefik_reverse-proxy_1/api/data", options)
        if (data.status !== 200) {
            res.status(data.status)
            res.send(data.data)
        } else {
            res.send(data.data)
        }
    } catch (e) {

        res.status(553);
        res.send("<p>core error</p>")
    }


});

router.post('/user', async (req, res, next) => {


    const header = {
        headers: {
            Host: "core"
        }
    }
    const payload = {
        token: req.body.token,
        user: req.body.user
    }
    try {
        const data = await axios.post("http://traefik_reverse-proxy_1/api/data", payload, header)
        if (data.status !== 200) {
            res.status(data.status)
            res.send(data.data)
        } else {
            res.status(200)
            res.send(data.data)
        }
    } catch (e) {
        res.status(553);
        res.send("<p>core error</p>")
    }
});

module.exports = router;
