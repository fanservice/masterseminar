const jose = require('jose')

class Authentication {

    constructor() {

    }

    async isTokenValid(token, keys) {
        try {
            this.publickey = jose.JWK.asKey(keys);
            return await jose.JWT.verify(token, this.publickey)

        } catch (e) {
            return false
        }

    }
}

module.exports = Authentication;
