var createError = require('http-errors');
var MyMongoDB = require('./databaseHandler/MyMongoDB');
const fs = require('fs')
const key = fs.readFileSync("authentication/mykeys.pub")


var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var loadkey = require('./authentication/loadkey');

let apiInterface = require('./routes/apiInterface.js')
//mongo db
const myMongoDB = new MyMongoDB();

var app = express();
const createDB = async () => {
    await myMongoDB.mongoConnect();
    await myMongoDB.preFillData();
}
createDB().then().catch(e => e)
// view engine setup
app.use(loadkey(key));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', apiInterface);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
