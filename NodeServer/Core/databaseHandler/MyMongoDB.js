//https://www.tutorialsteacher.com/nodejs/access-mongodb-in-nodejs
const { MongoClient } = require("mongodb");

class MyMongoDB {

    constructor() {
        this.uri = "mongodb://k:replaceMe@myMongo:27017/"
        this.client = new MongoClient(this.uri);
    }

    async mongoConnect() {
        try {
            return await this.client.connect()
        } catch (e) {

        }
    }

    async preFillData() {
        try {
            const client = await this.mongoConnect();
            const db = await client.db("myDB")
            const collection = await db.collection("users")
            const many = [];
            for (let i = 0; i < 100; i++) {
                many.push({ id: i, firstName: 'Kevin' + i, lastName: 'Wilhelm' + i })
            }
            await collection.insertMany(many);
        } catch (e) {
            ;
        }
    }

    async setUser(user) {
        try {
            const client = await this.mongoConnect();
            const db = await client.db("myDB")
            const collection = await db.collection("users")
            const query = { id: { $gte: 0 } }
            const finding = await collection.find(query)
            const length = await finding.count()
            return await collection.insert({ id: length + 1, firstName: user.name, lastName: user.lastname })
        } catch (e) {

        }

    }

    async getData() {
        try {
            const client = await this.mongoConnect();
            const db = await client.db("myDB")
            const collection = await db.collection("users")
            const i = Math.floor(Math.random() * 100);
            const findings = await collection.findOne({ id: i })
            return findings
        } catch (e) {

        }
    }

}


module.exports = MyMongoDB
