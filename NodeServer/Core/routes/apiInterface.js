const express = require('express');
const router = express.Router();
const db = require('../databaseHandler/MyMongoDB')
const auth = require('../authentication/authentication')

router.get('/api/data', async (req, res, next) => {
    try {
        const token = await req.query.token;
        const authenticateUser = await new auth().isTokenValid(token, req.keyfile)
        if (authenticateUser.user.role.find(d => d === "get")) {
            const myUserData = await new db().getData();
            if (myUserData) {
                res.send(myUserData);
            } else {

                res.status(553);
                res.send("<p>user probably not existing</p>")
            }
        } else {
            res.status(553);
            res.send("<p>user probably not existing or not allowed</p>")
        }

    } catch (e) {
        res.status(553);
        res.send("<p>core error</p>")
    }

});
router.post('/api/data/', async (req, res, next) => {
    const user = req.body.user;
    const token = req.body.token;
    try {
        const authenticateUser = await new auth().isTokenValid(token, req.keyfile)
        if (authenticateUser.user.role.find(d => d === "post")) {
            const setUser = await new db().setUser(user)
            if (setUser) {
                res.status(200);
                await res.send("user: " + user.name + " is created");
            } else {
                res.status(553);
                res.send("<p>check input</p>")
            }
        } else {
            res.status(553);
            res.send("<p>user probably not existing or not allowed</p>")
        }
    } catch (e) {
        res.status(553);
        res.send("<p>core error</p>")
    }
})

module.exports = router;
