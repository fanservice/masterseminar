var express = require('express');
var router = express.Router();
var axios = require('axios');

/* GET home page. */


router.get('/dashboard', async (req, res) => {
        const options = {
            params: {
                token: req.token
            },
            headers: {
                Host: "bend"
            }
        }
        try {
            const data = await axios.get("http://traefik_reverse-proxy_1/api/dashboard/users", options)
            if (data.status !== 200) {
                res.status(data.status)
                res.send("<p>error</p>")
            } else {
                await res.status(200)
                res.send("<p>" + data.data.id + " " + data.data.firstName + "</p>")
            }
        } catch (e) {
            if (e.response.status == 503) {
                res.status(552)
                res.send(e.response.data)
            } else {
                res.status(e.response.status)
                res.send(e.response.data)
            }
        }
    }
)

router.post('/dashboard', async (req, res) => {

        const header = {
            headers: {
                Host: "bend"
            }
        }
        const payload = {
            token: req.token,
            user: {name: "new Firstname", lastname: "new Lastname"}
        }

        try {
            const data = await axios.post("http://traefik_reverse-proxy_1/api/dashboard/user", payload, header)

            if (data.status !== 200) {
                res.status(data.status)
                res.send("<p>error</p>")
            } else {
                res.status(200)
                res.send("<p> user " + data.data + " erstellt</p>")
            }

        } catch (e) {
            if (e.response.status == 503) {
                res.status(552)
                res.send(e.response.data)
            } else {
                res.status(e.response.status)
                res.send(e.response.data)
            }
        }
    }
)

module.exports = router;
