const axios = require("axios")
module.exports = getAuth

function getAuth() {
    return async (req, res, next) => {
        const options = {
            "user": "kevin",
            "pass": "kevin"
        }
        const header = {
            headers: {
                Host: "bend"
            }
        }
        try {
            const token = await axios.post("http://traefik_reverse-proxy_1/api/token", options, header)
            if (token.status !== 200) {
                res.status(token.status)
                res.send("token error")
            } else {
                req.token = token.data.token
                next()
            }
        } catch (e) {

            if (e.response.status == 502) {
                res.status(552)
                res.send("<p>backend error</p>" + e.response.status)
            } else {
                res.status(e.response.status)
                res.send(e.response.data)
            }
        }

    }
}

