# MasterSeminar
# test
# Docker

Default Port beginning at 9000
Install docker latest and docker-compose latest, else you will get hash conc at names...

## Docker Register



docker run -p 9500:5000 registry
invalid and deleted for now

## Traefik 

### Setting up constraint
Constraints are for exclusion or inclusion of a docker instance.
only labels are used which are in the docker yaml of traefic
* eg
  * ``` constraints: "Label(`a.label.name`,`foo`)" ```

## Docker gitlab

- Port 9010-->80

  docker pull gitlab/gitlab-ce
  .gitlab-ci.yml added right now only test script is running
  will add docker container commands to start jmeter

## Docker Gitlab-Runner

Added runner to root group
Download the latest Docker Compose
Give docker compose permission 
```chmod +x /usr/local/bin/docker-compose```

- Ports:
  - 9330-->80

## Docker JMeter
In the Jmeter Docker folder start the main docker after that the slaves
docker-compose scale slave=n

to start the attack 
jmeter -n -t /home/simpleHTTPGETRequest.jmx -R[ip,ip]


## NodeApp

Basic HTTP Server nothing special

### Dependencies

* Docker Version 3.7+
* nmap

## Sources

- vulnarbilityBox for docker
  https://github.com/vulhub/vulhub
- Loadtesting Application
  https://www.radview.com/
- Gitlab Docker Configuration
  https://docs.gitlab.com/omnibus/docker/#install-gitlab-using-docker-compose
- Jmeter Docker Container
  https://www.vinsguru.com/jmeter-scaling-out-load-servers-using-docker-compose-in-distributed-load-testing/
- Docker Circuite Breaker
  https://hub.docker.com/r/grissomsh/hystrix-dashboard/
- Node Docker
  https://nodejs.org/de/docs/guides/nodejs-docker-webapp/
- NMAP
https://nmap.org/download.html
- SED replace all spaces
https://stackoverflow.com/questions/1251999/how-can-i-replace-a-newline-n-using-sed