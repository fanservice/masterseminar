docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' dockerjmeter_slave_1 > /masterdata/jmeter_ip/jmeter_ip_five
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' dockerjmeter_slave_2 >> /masterdata/jmeter_ip/jmeter_ip_five
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' dockerjmeter_slave_3 >> /masterdata/jmeter_ip/jmeter_ip_five
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' dockerjmeter_slave_4 >> /masterdata/jmeter_ip/jmeter_ip_five
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' dockerjmeter_slave_5 >> /masterdata/jmeter_ip/jmeter_ip_five
sed -i -e ':a;N;$!ba;s/\n/,/g' /masterdata/jmeter_ip/jmeter_ip_five
echo "$(</masterdata/jmeter_ip/jmeter_ip_five)"